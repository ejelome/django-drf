django-drf
==========

Django with DRF in 3.0

-------------------------------------------------------------------------------

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [django-drf](#django-drf)
    - [Setup](#setup)
        - [System](#system)
        - [Project](#project)
            - [Dependencies](#dependencies)
                - [Virtualenv](#virtualenv)
                - [Python](#python)
                - [Sample data](#sample-data)
    - [Usage](#usage)
        - [Serve](#serve)
    - [License](#license)
    - [Boilerplate](#boilerplate)
        - [Base](#base)
        - [Admin](#admin)
        - [Serializers](#serializers)
        - [Views](#views)
    - [Bonus](#bonus)
    - [Date](#date)

<!-- markdown-toc end -->

-------------------------------------------------------------------------------

Setup
-----

### System ###

### Project ###

#### Dependencies ####

##### Virtualenv #####

``` shell
$ pipenv shell
```

##### Python #####

``` shell
$ pipenv install
```

##### Sample data #####

``` shell
$ \
cd project/
./manage.py migrate
```

-------------------------------------------------------------------------------

Usage
-----

### Serve ###

``` shell
$ ./manage.py runserver
```

Then visit <http://localhost:8000>.

-------------------------------------------------------------------------------

License
-------

`django-drf` is licensed under [MIT].

-------------------------------------------------------------------------------

Boilerplate
-----------

### Base ###

1. Create a virtualenv:

   ``` shell
   $ pipenv --python 3.7
   ```

2. Switch to virtualenv:

   ``` shell
   $ pipenv shell
   ```
3. Install dependencies:

   ``` shell
   $ pipenv install django djangorestframework
   ```

4. Create a project:

   ``` shell
   $ django-admin startproject project
   ```

5. Change dir to project:

   ``` shell
   $ cd project/
   ```

6. Create the database:

   ``` shell
   $ ./manage.py migrate
   ```

7. Create a superuser:

   ``` shell
   $ ./manage.py createsuperuser
   ```

8. Create an app:

   ``` shell
   $ django-admin startapp posts
   ```

9. Update `project/settings.py`:

   ``` python
   ...
   INSTALLED_APPS = [
       ...
       'rest_framework',
       'posts',
   ]
   ...
   ```

10. Update `project/urls.py`:

    ``` python
    ...
    from django.urls import include, path

    urlpatterns = [
        ...
        path('', include('posts.urls')),
    ]
    ```

11. Create `posts/urls.py`:

    ``` python
    from django.urls import path, include

    urlpatterns = []
    ```

12. Update `posts/models.py`:

    ``` python
    ...
    class Post(models.Model):
        title = models.CharField(max_length=70)
        content = models.TextField()

        def __str__(self):
            return self.title
    ```

13. Create a migration file:

    ``` shell
    $ ./manage.py makemigrations
    ```

14. Update the database with the new table:

    ``` shell
    $ ./manage.py migrate
    ```

15. Run web server:

    ``` shell
    $ ./manage.py runserver
    ```

    Then visit http://localhost:8000.

### Admin ###

1. Update `posts/admin.py`:

    ``` python
    ...
    from .models import Post

    ...
    admin.site.register(Post)
    ```

    Then visit http://localhost:8000/admin/posts.

### Serializers ###

_The serializer translates to/from `Python` and `JSON` data._

**Notes:**

- To/from `Python`'s `list`s (`[]`) and `dict`s (`{}`)
- To/from `JSON`'s `array`s (`[]`) and  `object`s (`{}`)

1. Create `posts/serializers.py`:

   ``` python
   from rest_framework import serializers
   from .models import Post

   class PostSerializer(serializers.ModelSerializer):
       class Meta:
           model = Post
           fields = (
               'id',
               'title',
               'content',
           )
   ```

### Views ###

1. Update `posts/views.py`:

   ``` python
   ...
   from rest_framework import viewsets
   from .models import Post
   from .serializers import PostSerializer

   ...
   class PostView(viewsets.ModelViewSet):
       queryset = Post.objects.all()
       serializer_class = PostSerializer
   ```

   The `ModelViewSet` inherits from `GenericAPIView` which implements the actions, e.g.:

   - `.list()`
   - `.retrieve()`
   - `.create()`
   - `.update()`
   - `.partial_update()`
   - `.destroy()`

   <br />

2. Update `posts/urls.py`:

   ``` python
   ...
   from . import views
   from rest_framework import routers

   router = routers.DefaultRouter()
   router.register('posts', views.PostView)

   urlpatterns = [
       path('', include(router.urls)),
   ]
   ```

   Then visit http://localhost:8000.

-------------------------------------------------------------------------------

Bonus
-----

1. Update `posts/serializers.py`:

   ``` python
   ...

   class PostSerializer(serializers.HyperlinkedModelSerializer):
       class Meta:
           ...
           fields = (
               'url',
               'id',
               ...
           )
   ```

   **Notes:**

   - `HyperlinkedModelSerializer` is same as `ModelSerializer` but uses hyperlinks to represent relationships than primary keys
   - `HyperlinkedModelSerializer` serializer includes a `url` field instead of a primary key field
   - The `url` field value can be clicked to easily navigate to its corresponding resource

   <br />

-------------------------------------------------------------------------------

Date
----

`2020 Mar`

[MIT]: ./LICENSE
